# Ktor HTTP API

Sample HTTP API server with Ktor based on the article [Creating HTTP APIs](https://ktor.io/docs/creating-http-apis.html).

## License
[License](./LICENSE)
