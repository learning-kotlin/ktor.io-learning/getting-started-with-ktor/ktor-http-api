package org.elu.learn.ktor.plugins

import io.ktor.server.application.*
import io.ktor.server.routing.*
import org.elu.learn.ktor.routes.customerRouting
import org.elu.learn.ktor.routes.getOrderRoute
import org.elu.learn.ktor.routes.listOrdersRoute
import org.elu.learn.ktor.routes.totaliseOrderRoute

fun Application.configureRouting() {
    routing {
        customerRouting()
        listOrdersRoute()
        getOrderRoute()
        totaliseOrderRoute()
    }
}
